set(PQPluginHdrs
  pqSMTKProjectCloseBehavior.h
  pqSMTKProjectExportBehavior.h
  pqSMTKProjectImportModelBehavior.h
  pqSMTKProjectMenu.h
  pqSMTKProjectNewBehavior.h
  pqSMTKProjectOpenBehavior.h
  pqSMTKProjectSaveBehavior.h
  pqSMTKProjectLoader.h
  pqSMTKRecentProjectsMenu.h
  )

set(PQPluginSrcs
  pqSMTKProjectCloseBehavior.cxx
  pqSMTKProjectExportBehavior.cxx
  pqSMTKProjectImportModelBehavior.cxx
  pqSMTKProjectMenu.cxx
  pqSMTKProjectNewBehavior.cxx
  pqSMTKProjectOpenBehavior.cxx
  pqSMTKProjectSaveBehavior.cxx
  pqSMTKProjectLoader.cxx
  pqSMTKRecentProjectsMenu.cxx
  )

set(CMAKE_AUTOMOC 1)

# Add "Project" menu to application
paraview_plugin_add_action_group(
  CLASS_NAME pqSMTKProjectMenu
  GROUP_NAME "MenuBar/Project"
  INTERFACES interfaces
  SOURCES sources
  )

# Specify the plugin
paraview_add_plugin(smtkProjectManagerPlugin
  VERSION "1.0"
  UI_INTERFACES  ${interfaces}
  SOURCES
    ${PQPluginHdrs}
    ${PQPluginSrcs}
    ${sources}
  )

target_link_libraries(smtkProjectManagerPlugin
  LINK_PUBLIC
    projectConfig
    smtkCore
    smtkPQComponentsExt
    ParaView::pqApplicationComponents
    ParaView::pqComponents
    ${Boost_LIBRARIES}
    Qt5::Core
  )

target_include_directories(smtkProjectManagerPlugin PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
)
