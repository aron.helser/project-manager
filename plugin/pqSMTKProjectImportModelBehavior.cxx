//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectImportModelBehavior.h"

#include "pqSMTKProjectLoader.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// Client side
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QMessageBox>
#include <QString>

//-----------------------------------------------------------------------------
pqProjectImportModelReaction::pqProjectImportModelReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectImportModelReaction::importModel()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);

  // Get the current project
  auto projectManager = wrapper->smtkProjectManager();
  auto project = projectManager->getCurrentProject();
  if (project == nullptr)
  {
    qWarning() << "Cannot import model because no project currently loaded.";
    return;
  }

  // Construct a file dialog for the user to select the model file
  QString filter("Exodus Files (*.ex? *.gen);;NetCDF Files (*.ncdf);;All Files (*)");
  pqFileDialog fileDialog(
    server, pqCoreUtilities::mainWidget(), tr("Select Model File:"), QString(), filter);
  fileDialog.setObjectName("FileOpenDialog");
  fileDialog.setFileMode(pqFileDialog::ExistingFile);
  fileDialog.setShowHidden(true);
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }

  QString location = fileDialog.getSelectedFiles()[0];
  // Currently can only add "second" model
  bool copyNativeFile = true;
  bool useVTKSession = true;
  bool success = project->addModel(location.toStdString(), "second", copyNativeFile, useVTKSession);

  if (success)
  {
    QMessageBox::information(
      pqCoreUtilities::mainWidget(), "Success", "Added second model to project.");
  }
  else
  {
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error",
      "Unable to load model; see \"Output Messages\" for more info.");
  }
} // importModel()

//-----------------------------------------------------------------------------
static pqSMTKProjectImportModelBehavior* g_instance = nullptr;

pqSMTKProjectImportModelBehavior::pqSMTKProjectImportModelBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectImportModelBehavior* pqSMTKProjectImportModelBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectImportModelBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectImportModelBehavior::~pqSMTKProjectImportModelBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
