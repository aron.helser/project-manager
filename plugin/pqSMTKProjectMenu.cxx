//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectMenu.h"

#include "pqSMTKProjectCloseBehavior.h"
#include "pqSMTKProjectExportBehavior.h"
#include "pqSMTKProjectImportModelBehavior.h"
#include "pqSMTKProjectLoader.h"
#include "pqSMTKProjectNewBehavior.h"
#include "pqSMTKProjectOpenBehavior.h"
#include "pqSMTKProjectSaveBehavior.h"
#include "pqSMTKRecentProjectsMenu.h"

#include "smtk/model/Resource.h"
#include "smtk/project/Project.h"

#include "pqApplicationCore.h"

#include <QAction>
#include <QDebug>
#include <QMenu>
#include <QString>
#include <QtGlobal> // qWarning()

//-----------------------------------------------------------------------------
pqSMTKProjectMenu::pqSMTKProjectMenu(QObject* parent)
  : Superclass(parent)
  , m_newProjectAction(nullptr)
  , m_openProjectAction(nullptr)
  , m_closeProjectAction(nullptr)
  , m_saveProjectAction(nullptr)
  , m_exportProjectAction(nullptr)
  , m_recentProjectsMenu(nullptr)
  , m_importModelAction(nullptr)
{
  this->startup();
}

//-----------------------------------------------------------------------------
pqSMTKProjectMenu::~pqSMTKProjectMenu()
{
  this->shutdown();
}

//-----------------------------------------------------------------------------
bool pqSMTKProjectMenu::startup()
{
  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "cannot initialize Project menu because pqCore is not found";
    return false;
  }

  // Access/create the singleton instances of our behaviors.
  auto newProjectBehavior = pqSMTKProjectNewBehavior::instance(this);
  auto openProjectBehavior = pqSMTKProjectOpenBehavior::instance(this);
  auto saveProjectBehavior = pqSMTKProjectSaveBehavior::instance(this);
  auto closeProjectBehavior = pqSMTKProjectCloseBehavior::instance(this);
  auto exportProjectBehavior = pqSMTKProjectExportBehavior::instance(this);
  auto importModelBehavior = pqSMTKProjectImportModelBehavior::instance(this);

  // Register our behaviors with the application core.
  pqCore->registerManager("smtk new project", newProjectBehavior);
  pqCore->registerManager("smtk open project", openProjectBehavior);
  pqCore->registerManager("smtk save project", saveProjectBehavior);
  pqCore->registerManager("smtk close project", closeProjectBehavior);
  pqCore->registerManager("smtk export project", exportProjectBehavior);
  pqCore->registerManager("smtk project import model", importModelBehavior);

  // Initialize "New Project" action
  m_newProjectAction = new QAction(tr("New Project..."), this);
  auto newProjectReaction = new pqProjectNewReaction(m_newProjectAction);
  QObject::connect(newProjectReaction, &pqProjectNewReaction::projectCreated, this,
    &pqSMTKProjectMenu::onProjectOpened);

  // Initialize "Open Project" action
  m_openProjectAction = new QAction(tr("Open Project..."), this);
  auto openProjectReaction = new pqProjectOpenReaction(m_openProjectAction);

  // Initialize "Recent Projects" menu
  m_recentProjectsAction = new QAction(tr("Recent Projects"), this);
  QMenu* menu = new QMenu();
  m_recentProjectsAction->setMenu(menu);
  m_recentProjectsMenu = new pqSMTKRecentProjectsMenu(menu, menu);

  // Initialize "Import Model" action
  m_importModelAction = new QAction(tr("Import Second Model..."), this);
  m_importModelAction->setEnabled(false);
  auto importModelReaction = new pqProjectImportModelReaction(m_importModelAction);

  // Initialize "Save Project" action
  m_saveProjectAction = new QAction(tr("Save Project"), this);
  auto saveProjectReaction = new pqProjectSaveReaction(m_saveProjectAction);

  // Initialize "Close Project" action
  m_closeProjectAction = new QAction(tr("Close Project"), this);
  auto closeProjectReaction = new pqProjectCloseReaction(m_closeProjectAction);
  QObject::connect(closeProjectReaction, &pqProjectCloseReaction::projectClosed, this,
    &pqSMTKProjectMenu::onProjectClosed);

  // Initialize "Export Project" action
  m_exportProjectAction = new QAction(tr("Export Project"), this);
  auto exportProjectReaction = new pqProjectExportReaction(m_exportProjectAction);

  // For now, presume that there is no project loaded at startup
  this->onProjectClosed();

  // Connect to project loader
  auto projectLoader = pqSMTKProjectLoader::instance();
  QObject::connect(
    projectLoader, &pqSMTKProjectLoader::projectOpened, this, &pqSMTKProjectMenu::onProjectOpened);

  return true;
} // startup()

//-----------------------------------------------------------------------------
void pqSMTKProjectMenu::shutdown()
{
  // Unregister our behaviors from the application core.
  auto pqCore = pqApplicationCore::instance();
  if (pqCore)
  {
    pqCore->unRegisterManager("smtk new project");
    pqCore->unRegisterManager("smtk open project");
    pqCore->unRegisterManager("smtk save project");
    pqCore->unRegisterManager("smtk close project");
    pqCore->unRegisterManager("smtk export project");
    pqCore->unRegisterManager("smtk project import model");
  }
}

//-----------------------------------------------------------------------------
void pqSMTKProjectMenu::onProjectOpened(smtk::project::ProjectPtr project)
{
  m_newProjectAction->setEnabled(false);
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_saveProjectAction->setEnabled(true);
  m_closeProjectAction->setEnabled(true);
  m_exportProjectAction->setEnabled(true);

  auto secondModel = project->findResource<smtk::model::Resource>("second");
  m_importModelAction->setEnabled(secondModel == nullptr);
}

//-----------------------------------------------------------------------------
void pqSMTKProjectMenu::onProjectClosed()
{
  m_newProjectAction->setEnabled(true);
  m_openProjectAction->setEnabled(true);
  m_recentProjectsAction->setEnabled(true);
  m_saveProjectAction->setEnabled(false);
  m_closeProjectAction->setEnabled(false);
  m_exportProjectAction->setEnabled(false);
  m_importModelAction->setEnabled(false);
}
