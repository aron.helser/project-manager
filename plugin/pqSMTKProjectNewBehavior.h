//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectNewBehavior_h
#define pqSMTKProjectNewBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqProjectNewReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectNewReaction(QAction* parent);

  void newProject();

signals:
  void dialogValid(bool);
  void projectCreated(smtk::project::ProjectPtr);

protected slots:
  void onModifiedParameters();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->newProject(); }

private:
  Q_DISABLE_COPY(pqProjectNewReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKProjectNewBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKProjectNewBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKProjectNewBehavior() override;

protected:
  pqSMTKProjectNewBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKProjectNewBehavior);
};

#endif
