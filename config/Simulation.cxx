//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "Simulation.h"

#include "smtk/attribute/FileItem.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/project/Project.h"

#include "boost/filesystem.hpp"

#include <string>

namespace config
{

bool Simulation::setModelFile(smtk::attribute::FileItemPtr fileItem,
  smtk::project::ProjectPtr project, const std::string& modelIdentifier,
  smtk::io::Logger& logger) const
{
  // Check all vars
  if ((fileItem == nullptr) || (project == nullptr))
  {
    return false;
  }

  auto modelResource = project->findResource<smtk::model::Resource>(modelIdentifier);
  if (modelResource == nullptr)
  {
    return false;
  }

  fileItem->setIsEnabled(true);
  std::string importLocation = project->importLocation(modelResource);
  if (!importLocation.empty())
  {
    // Check project directory for the same filename
    boost::filesystem::path projectDir(project->directory());
    boost::filesystem::path importPath(importLocation);
    auto projectMeshPath = projectDir / importPath.filename();
    if (boost::filesystem::exists(projectMeshPath))
    {
      fileItem->setValue(0, projectMeshPath.string());
    }
    else if (boost::filesystem::exists(importPath))
    {
      fileItem->setValue(0, importPath.string());
    }
    else
    {
      std::stringstream ss;
      ss << "Unable to find mesh file at \"" << importPath.string() << "\"";
      smtkWarningMacro(logger, ss.str());
    }
  } // if (importLocation)

  return true;
}

} // namespace config
