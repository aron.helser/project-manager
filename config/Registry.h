//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef project_config_Registry_h
#define project_config_Registry_h

#include "config/Exports.h"

#include "config/Simulation.h"

#include "smtk/PublicPointerDefs.h"

#include <string>

namespace config
{
/// A static class for initializing simulation-specific configuration
/// instances.
class PROJECTCONFIG_EXPORT Registry
{
public:
  static smtk::shared_ptr<Simulation> getConfig(const std::string& simulationCode);
};

} // namespace config

#endif
