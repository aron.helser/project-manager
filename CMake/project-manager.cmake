# A contract file for testing the Project Manager extension within SMTK.

cmake_minimum_required(VERSION 3.8.2)
project(project-manager)

include(ExternalProject)

ExternalProject_Add(project-manager
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/project-manager.git"
  GIT_TAG "origin/master"
  PREFIX plugin
  STAMP_DIR plugin/stamp
  SOURCE_DIR plugin/src
  BINARY_DIR plugin/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_TESTING=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_BEFORE_INSTALL True
)
